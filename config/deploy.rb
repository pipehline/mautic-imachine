# config valid for current version and patch releases of Capistrano
lock "~> 3.10.1"

set :application, 'mautic'
set :repo_url, 'git@bitbucket.org:pipehline/mautic-imachine.git'

# Default branch is :master
ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/html/mautic-imachine'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, 'config/database.yml', 'config/secrets.yml'

# Default value for linked_dirs is []
# append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system'

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

namespace :deploy do
  desc "Set deployer user"
  task :deployer_user do
    on roles(:app), in: :sequence, wait: 5 do
      execute "sudo chown -Rf deployer:deployer /var/www/html/mautic-imachine"
    end
  end

  before :starting, :deployer_user

  desc "Git init"
  task :git_init do
    on roles(:app), in: :sequence, wait: 5 do
      execute "cd #{release_path} && git init"
    end
  end

  after :publishing, :git_init

  desc "Composer install"
  task :composer_install do
    on roles(:app), in: :sequence, wait: 5 do
      execute "cd #{release_path} && composer install"
    end
  end

  after :git_init, :composer_install

  desc "Clear mautic cache"
  task :clear_cache do
    on roles(:app), in: :sequence, wait: 5 do
      execute "cd #{release_path} && php app/console cache:clear --env=prod"
    end
  end

  after :composer_install, :clear_cache

  desc "Restart servers"
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute "sudo service nginx restart && sudo service php7.0-fpm restart"
    end
  end

  after :composer_install, :restart

  desc "Set www-data"
  task :www_user do
    on roles(:app), in: :sequence, wait: 5 do
      execute "sudo chown -Rf www-data:www-data /var/www/html/mautic-imachine"
    end
  end

  after :finished, :www_user

end
